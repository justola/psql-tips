    <div class="tip">Using the \l metacommand without a pattern will show a list
    of all visible databases.
      <pre><code class="hljs bash">laetitia=# \l
                                            List of databases
   Name    |  Owner   | Encoding | Collate | Ctype | ICU Locale | Locale Provider |   Access privileges   
-----------+----------+----------+---------+-------+------------+-----------------+-----------------------
 laetitia  | laetitia | UTF8     | C       | UTF-8 |            | libc            | 
 postgres  | postgres | UTF8     | C       | UTF-8 |            | libc            | 
 template0 | postgres | UTF8     | C       | UTF-8 |            | libc            | =c/postgres          +
           |          |          |         |       |            |                 | postgres=CTc/postgres
 template1 | postgres | UTF8     | C       | UTF-8 |            | libc            | =c/postgres          +
           |          |          |         |       |            |                 | postgres=CTc/postgres
(4 rows)</code></pre>This feature is available since
      at least Postgres 7.1, but was updated with postgres 8.0, Postgres 8.1
      and Postgres 9.3.
	</div>
