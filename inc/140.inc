    <div class="tip">The <code>\h command</code> metacommand will give the syntax of the
    specified SQL command.
      <pre><code class="hljs bash">laetitia=# \h cluster
Command:     CLUSTER
Description: cluster a table according to an index
Syntax:
CLUSTER [VERBOSE] table_name [ USING index_name ]
CLUSTER ( option [, ...] ) table_name [ USING index_name ]
CLUSTER [VERBOSE]

where option can be one of:

    VERBOSE [ boolean ]

URL: https://www.postgresql.org/docs/15/sql-cluster.html</code></pre>This feature is available since
      at least Postgres 7.1.
	</div>
